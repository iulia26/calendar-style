let nav = 0;//a way to keep track of 
//whichever month we're lookin up
let clicked = null;//data pe care am facut click
let events = localStorage.getItem('events') ? JSON.parse(localStorage.getItem('events')) : [];

const calendar = document.getElementById('calendar');
const newEventModal = document.getElementById('newEventModal');
const deleteEventModal = document.getElementById('deleteEventModal');
const backDrop = document.getElementById('modalBackDrop');
const eventTitleInput = document.getElementById('eventTitleInput');
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];



function openModal(date) {
    clicked = date;//The date on which the user clicked on
    
    //if (events != [])
        eventsForDay = events.filter(e => e.date === date);
   // else
       // eventsForDay = [];
    cnt = eventsForDay.length;
    document.getElementById('eventText').innerText = "";
    document.getElementById('checkBoxSpace').innerHTML = '';
    document.getElementById('currentEvents').innerText = "";
    EventsString = "";
    
    if (cnt > 0) {

        while (cnt > 0) {
            EventTitle = eventsForDay[cnt - 1].title;
            
            //Create a checkbox  for each event of the day

            

               
            var label = document.createElement("label");//creating a label,the first step we need to to before
            //we get in touch with the css class which have already been defined
            label.className = "container";//we associate label with the class created in the css file

            //we add the span
            var span = document.createElement("span");
            span.className = "checkmark";

            //description of the checkbox,in our case it will be the Event Title
            var description = document.createTextNode(EventTitle);
            var checkbox = document.createElement('input');
                    checkbox.type = "checkbox";
                    checkbox.name = "checkbox";
                    checkbox.value = EventTitle;
                    checkbox.id = "id"+cnt;//ERROR>???????????????????????


           


            label.appendChild(checkbox);
            label.appendChild(description);
            label.appendChild(span);

            var checkBoxArea = document.getElementById('checkBoxSpace');
         

            checkBoxArea.appendChild(label);
            
            cnt--;

        }
        console.log(EventsString);
       
       
        newEventModal.style.display = 'block';
        //deleteEventModal.style.display = 'block';

       
    }
    else {
        
            newEventModal.style.display = 'block';

            document.getElementById('currentEvents').innerText = "0";
        
        }
   
    backDrop.style.display = 'block';
    
}


function load() {
    
    const dt = new Date();//we assign a data object
   
    if (nav !== 0) {
        dt.setMonth(new Date().getMonth() + nav);
    }
    
    const day = dt.getDate();
    const month = dt.getMonth();
    const year = dt.getFullYear();

    const firstDayOfMonth = new Date(year,month,1);
    const daysInMonth = new Date(year, month + 1, 0).getDate();//gives the last day of the current month

    const dateString = firstDayOfMonth.toLocaleDateString('en-us', {

        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day:'numeric'



    });

    const paddingDays = weekdays.indexOf(dateString.split(', ')[0]);

    document.getElementById('monthDisplay').innerText = `${dt.toLocaleDateString(
        'en-us', { month: 'long' })
        } ${year}`;
    calendar.innerHTML = '';
    for (let i = 1; i <= paddingDays + daysInMonth; i++) {
        const daySquare = document.createElement('div');
        daySquare.classList.add('day');

        if (i > paddingDays) {

            //We have ittereted through our paddig days alrdeay
            daySquare.innerText = i - paddingDays;

            const dayString = `${month + 1}/${i - paddingDays}/${year}`;

           
                eventsForDay = events.find(e => e.date === dayString);
                if (eventsForDay) {
                    eventDiv = document.createElement('div');
                    eventDiv.classList.add('event');

                    eventDiv.innerText = "Busy";//set the text
                    daySquare.appendChild(eventDiv);
                }
                daySquare.addEventListener('click', () => openModal(dayString));

            } else {
                daySquare.classList.add('padding');
            }

            calendar.appendChild(daySquare);

        

    }

    console.log(paddingDays);
}

function closeModal() {
    eventTitleInput.classList.remove('error');
    newEventModal.style.display = 'none';
    deleteEventModal.style.display = 'none';
    backDrop.style.display = 'none';
    eventTitleInput.value = '';
    clicked = null;
    load();
}

function saveEvent() {
    var date = clicked + "";
    if (eventTitleInput.value != "") {
        eventTitleInput.classList.remove('error');
        events.push({
            date: clicked,
            title: eventTitleInput.value

        });
        localStorage.setItem('events', JSON.stringify(events));

        closeModal();
        console.log(date);
        openModal(date);

    }
    else {
        eventTitleInput.classList.add('error');
    }
    
}

function remove_array_item(arr, title) {

    for (var i = 0; i < arr.length; i++) {
        if (arr[i].title == title) {
            arr.splice(i, 1);
           
        }
            
    }
    
}

function deleteEvent() {

    
    var iterator = document.getElementById('checkBoxSpace').getElementsByTagName('input');

    var i = 0, current = 0, date = clicked + "";
    
    while (current = iterator[i++]) {

        if (current.type == 'checkbox' && current.checked) {

          
          
           remove_array_item(events, current.value);
         
            
        }
    }
    
   

    localStorage.setItem('events', JSON.stringify(events));

    closeModal();
    openModal(date);
    

    
   


}
function initButtons() {

    document.getElementById('nextButton').
        addEventListener('click', () => {
            nav++;
            
            load();


        });

    document.getElementById('backButton').
        addEventListener('click', () => {
            nav--;
            load();


        });

    document.getElementById('saveButton').addEventListener('click', saveEvent);

    document.getElementById('cancelButton').addEventListener('click', closeModal);

    document.getElementById('deleteButton').addEventListener('click', deleteEvent);

    document.getElementById('closeButton').addEventListener('click', closeModal);

}

initButtons();
load();//observer
